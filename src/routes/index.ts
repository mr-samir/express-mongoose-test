import { ReadingRoute } from '@routes/reading.route';

const routes = [new ReadingRoute()];

export default routes;
